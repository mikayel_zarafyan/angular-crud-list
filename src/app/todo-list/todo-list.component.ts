import { Component, OnInit } from '@angular/core';
import { DeleteTodo, GetTodos, SetSelectedTodo, UpdateTodo } from '../todo/todo.actions';
import { TodoState } from '../todo/todo.state';
import { Select, Store } from '@ngxs/store';
import { Todo } from '../todo/Todo';
import { Observable } from 'rxjs';
import { STATUS, TABS } from '../constants'

@Component({
    selector: 'app-todo-list',
    templateUrl: './todo-list.component.html',
    styleUrls: ['./todo-list.component.scss']
})

export class TodoListComponent implements OnInit {

    tab = TABS.all;

    todoStatus = null;

    status = STATUS

    tabs = TABS

    @Select(TodoState.getTodoList) todos: Observable<Todo[]>;

    constructor(private store: Store) {
    }

    ngOnInit() {
        this.store.dispatch(new GetTodos());
    }

    deleteTodo(id: number) {
        this.store.dispatch(new DeleteTodo(id));
    }

    editTodo(payload: Todo) {
        this.store.dispatch(new SetSelectedTodo(payload));
    }

    completeTodo(event, payload: Todo ) {
        if(payload.status === STATUS.archived) {
            return false;
        }
        if(event.target.checked){
            payload.status = STATUS.completed
        } else {
            payload.status = STATUS.pending
        }
        this.store.dispatch(new UpdateTodo(payload, payload.id))
    }

    restoreFromArchive(payload: Todo) {
        payload.status = STATUS.pending

        this.store.dispatch(new UpdateTodo(payload, payload.id))
    }

    addToArchive(payload: Todo) {
        payload.status = STATUS.archived

        this.store.dispatch(new UpdateTodo(payload, payload.id))
    }

    select(setTab) {
        this.tab = setTab;
        if (setTab === TABS.pending)
            this.todoStatus = STATUS.pending;
        else if (setTab === TABS.completed)
            this.todoStatus = STATUS.completed;
        else if (setTab === TABS.archived)
            this.todoStatus = STATUS.archived;
        else
            this.todoStatus = null;
    }

    isSelected(checkTab) {
        return (this.tab === checkTab);
    }
}