import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { TodoState } from '../todo/todo.state';
import { AddTodo, SetSelectedTodo, UpdateTodo } from '../todo/todo.actions';
import { Observable } from 'rxjs';
import { Todo } from '../todo/Todo';
import { STATUS } from '../constants'

@Component({
	selector: 'app-todo-form',
	templateUrl: './todo-form.component.html',
	styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {

	@Select(TodoState.getSelectedTodo) selectedTodo: Observable<Todo>;

    todoForm: FormGroup;

    editTodo = false;

    constructor(private fb: FormBuilder, private store: Store) {
        this.createForm();
    }

    ngOnInit() {
        this.selectedTodo.subscribe(todo => {
            if (todo) {
                this.todoForm.patchValue({
                    id: todo.id,
                    status: todo.status,
                    title: todo.title
                });
                this.editTodo = true;
            } else {
                this.editTodo = false;
            }
        });
    }

    createForm() {
        this.todoForm = this.fb.group({
        	id: [null],
            status: [STATUS.pending],
            title: ['', Validators.required]
        });
    }

    onSubmit() {
    	if(!this.todoForm.value.status) {
    		this.todoForm.value.status = STATUS.pending;
    	}
    	
        if (this.editTodo) {
            this.store.dispatch(new UpdateTodo(this.todoForm.value, this.todoForm.value.id)).subscribe(() => {
                this.clearForm();
            });
        } else {
            this.store.dispatch(new AddTodo(this.todoForm.value)).subscribe(() => {
                this.clearForm();
            });
        }
    }

    clearForm() {
        this.todoForm.reset();
        this.store.dispatch(new SetSelectedTodo(null));
    }
}