const STATUS = {
    pending: 1,
    completed: 2,
    archived: 3
}

const TABS = {
    all: 1,
    pending: 2,
    completed: 3,
    archived: 4
}

export {
    STATUS,
    TABS
}