import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';

import { AppComponent } from './app.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoState } from './todo/todo.state';
import { HttpClientModule } from '@angular/common/http';
import { FilteredTodosPipe } from './pipes/filtered-todos.pipe';
import { InMemoryWebApiModule } from "angular-in-memory-web-api";  
import { DataService } from "./services/data.service";
import { TodoFormComponent } from './todo-form/todo-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    FilteredTodosPipe,
    TodoFormComponent
  ],
  imports: [
    BrowserModule,
    NgxsModule.forRoot([
        TodoState
    ]),
    NgxsReduxDevtoolsPluginModule,
    NgxsLoggerPluginModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(DataService, { delay: 100 }),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }