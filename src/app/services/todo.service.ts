import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../todo/Todo';

@Injectable({
    providedIn: 'root'
})
export class TodoService {

    SERVER_URL: string = "http://localhost:4200/api/";

    constructor(private http: HttpClient) {
    }

    fetchTodos() {
        return this.http.get<Todo[]>(this.SERVER_URL + 'todos');
    }

    deleteTodo(id: number) {
        return this.http.delete(`${this.SERVER_URL + 'todos'}/${id}`)
    }

    addTodo(payload: Todo) {
        return this.http.post<Todo>(`${this.SERVER_URL + 'todos'}`, payload)
    }

    updateTodo(payload: Todo, id: number) {
        return this.http.put<Todo>(`${this.SERVER_URL + 'todos'}/${id}`, payload)
    }
}