import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api'
import { Todo } from '../todo/Todo';

@Injectable({
	providedIn: 'root'
})
export class DataService implements InMemoryDbService {

	constructor() { }

	createDb(){

	   	let  todos =  [
	    	{  id:  1,  status: 1, title: 'TODO 1' },
	    	{  id:  2,  status: 1, title: 'TODO 2' },
	    	{  id:  3,  status: 1, title: 'TODO 3' },
	    	{  id:  4,  status: 2, title: 'TODO 4' },
	    	{  id:  5,  status: 2, title: 'TODO 5' },
	    	{  id:  6,  status: 2, title: 'TODO 6' },
	    	{  id:  7,  status: 3, title: 'TODO 7' },
	    	{  id:  8,  status: 3, title: 'TODO 8' },
	    	{  id:  9,  status: 3, title: 'TODO 9' },
	    	{  id:  10,  status: 3, title: 'TODO 10' }
	   	];

	   	return { todos };
  	}

  	genId(todos: Todo[]): number {
        return todos.length > 0 ? Math.max(...todos.map(todo => todo.id)) + 1 : 1;
    }
}
